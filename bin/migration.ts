#!/usr/bin/env node

import Pino from "pino";

import { createMigrationFile } from "../src/create_migration_file";
import { applyMigrations } from "../src/apply_migrations";
import { resetMigrations } from "../src/reset_migrations";

export const MIGRATIONS_DIR = `${process.cwd()}/migrations`;

export const logger: Pino.Logger = Pino();

const actionsMap: Record<string, Function> = {
  create: createMigrationFile,
  apply: applyMigrations,
  reset: resetMigrations,
};

const migration = async () => {
  const action = process.argv[2];
  if (!actionsMap.hasOwnProperty(action)) {
    logger.fatal("Action invalide.");
    process.exit(9);
  }
  const fn = actionsMap[action];
  await fn();
};
migration();