# Simple Postgres Migrations

A simple library to manage Postgres databases migrations using raw SQL files.

## How to use
1. Firstly, setup the needed environment variables :

* `PGHOST` : the host on which the Postgres service runs
* `PGPORT` : the port on which the Postgres service runs
* `PGUSER` : the database's user
* `PGPASSWORD` : the user's password
* `PGDATABASE`: the database's name

2. Create an empty SQL migration file (also create a migration directory in your project root if it doesn't exists) : <code>npx migration create *filename*</code>.
This creates a file named <code>*YYYMMdd*-*filename*.sql</code>.
3. Fill in your sql commands.
4. Then run the following command to apply the content of your migrations directory : `npx migrations apply`.

If you just cloned a project using this library, you can just apply the previous migrations using `npx migrations apply` to setup your schema accordingly.

## How do this works

Migrations are persisted in a table named "migrations" in your database. This table contains a single column which is the name of the file which had been applied.
This means that each run of the command  `npx migrations apply` fills in a new entry in your database table.

## Reset the migrations table

You can empty the migrations table to force a re-application of the migrations files with the following command : `npx migrations reset`.
Do note however that this doesn't undo the previously applied migrations, so you may encounter conflicts at some point when you run `npx migrations apply`.
