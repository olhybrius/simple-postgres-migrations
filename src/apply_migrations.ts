import { logger, MIGRATIONS_DIR } from "../bin/migration";
import { sql } from "./db";
import * as fs from "fs";
import * as process from "process";

const fsPromises = fs.promises;

export const applyMigrations = async () => {
  try {
    await fsPromises.access(MIGRATIONS_DIR); // check if directory exists
    const createMigrationsTableQuerry =
      "CREATE TABLE IF NOT EXISTS migrations (file VARCHAR (255) PRIMARY KEY)";
    await sql`${createMigrationsTableQuerry}`;
    const appliedMigrations = (await sql`SELECT file FROM migrations`).map(
      (row) => row.file
    );
    const migrationFiles = await fsPromises.readdir(MIGRATIONS_DIR);

    const migrationsToApply = migrationFiles.filter(
      (migration) => !appliedMigrations.includes(migration)
    );

    for (const migration of migrationsToApply) {
      await sql.file(`${MIGRATIONS_DIR}/${migration}`);
      await sql`INSERT INTO migrations (file)
                      VALUES (${migration})`;
    }

    await sql.end();
  } catch (error) {
    logger.fatal(error);
    process.exit(1);
  }
};
