import { logger, MIGRATIONS_DIR } from '../bin/migration';
import * as fs from 'fs';

const fsPromises = fs.promises;

export const createMigrationFile = async () => {
  const fileName = process.argv[3];

  if (!fileName) {
    logger.fatal('Veuillez spécifier un nom de fichier.');
    process.exit(9);
  }
  await createDirectoryIfNotExists(MIGRATIONS_DIR);
  const prefix = createDateTimeString();
  const migrationFileName = `${prefix}-${fileName}.sql`;
  await fsPromises.open(`${MIGRATIONS_DIR}/${migrationFileName}`, 'w');
};

const createDirectoryIfNotExists = async (path: string) => {
  try {
    await fsPromises.access(path);
  } catch (error) {
    await fsPromises.mkdir(path);
  }
};

const createDateTimeString = () => {
  const now = new Date();
  const year = now.getFullYear();
  const month = toTwoDigitsString(now.getMonth() + 1);
  const day = toTwoDigitsString(now.getDate());
  const hours = toTwoDigitsString(now.getHours());
  const minutes = toTwoDigitsString(now.getMinutes());
  const seconds = toTwoDigitsString(now.getSeconds());
  return `${year}${month}${day}${hours}${minutes}${seconds}`;
};

const toTwoDigitsString = (number: number) => {
  return number.toString().padStart(2, '0');
};
