import { sql } from "./db";

export const resetMigrations = async () => {
  await sql`TRUNCATE table migrations`;
  await sql.end();
};
