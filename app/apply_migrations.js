const fs = require("fs");
const {sql} = require("./db");

const fsPromises = fs.promises;


const applyMigrations = async () => {
    try {
        await fsPromises.access('migrations'); // check if directory exists
        await sql`CREATE TABLE IF NOT EXISTS migrations
        (
            file VARCHAR
                  (
            255
                  ) PRIMARY KEY)`;
        const appliedMigrations = (await sql`SELECT file
                                         FROM migrations`).map(row => row.file);
        const migrationFiles = await fsPromises.readdir('migrations');

        const migrationsToApply = migrationFiles.filter(migration => !appliedMigrations.includes(migration));

        for (const migration of migrationsToApply) {
            await sql.file(`migrations/${migration}`);
            await sql`INSERT INTO migrations (file)
                      VALUES (${migration})`;
        }

        await sql.end();
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}

module.exports = {
    applyMigrations
}
