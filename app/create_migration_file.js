const fs = require("fs");
const fsPromises = fs.promises;

const createMigrationFile = async () => {
    const fileName = process.argv[3];

    if (!fileName) {
        console.error('Veuillez spécifier un nom de fichier.');
        process.exit(1);
    }
    await createDirectoryIfNotExists('migrations');
    const prefix = createDateTimeString();
    const migrationFileName = `${prefix}-${fileName}.sql`;
    await fsPromises.open(`migrations/${migrationFileName}`, 'w');
    return migrationFileName;
}

const createDirectoryIfNotExists = async (path) => {
    try {
        await fsPromises.access(path);
    } catch (error) {
        await fsPromises.mkdir(path);
    }
}

const createDateTimeString = () => {
    const now = new Date();
    const year = now.getFullYear();
    const month = toTwoDigitsString(now.getMonth() + 1);
    const day = toTwoDigitsString(now.getDate());
    const hours = toTwoDigitsString(now.getHours());
    const minutes = toTwoDigitsString(now.getMinutes());
    const seconds = toTwoDigitsString(now.getSeconds());
    return `${year}${month}${day}${hours}${minutes}${seconds}`;
}

const toTwoDigitsString = (number) => {
    return number.toString().padStart(2, "0");
}

module.exports = {
    createMigrationFile
}
