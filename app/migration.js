const {createMigrationFile} = require("./create_migration_file");
const {applyMigrations} = require("./apply_migrations");
const {resetMigrations} = require("./reset_migrations");

const actionsMap = {
    'create': createMigrationFile ,
    'apply': applyMigrations,
    'reset': resetMigrations
};

const migration = async () => {
    const action = process.argv[2];
    if (!actionsMap.hasOwnProperty(action)) {
        console.error('Action invalide.')
        process.exit(1);
    }
    const fn = actionsMap[action];
    await fn();
}
migration();
