const postgres = require("postgres");
require('dotenv').config();

const sql = postgres({});

module.exports = {
    sql
}
