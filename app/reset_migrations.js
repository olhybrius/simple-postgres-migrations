const {sql} = require("./db");

const resetMigrations = async () => {
    await sql`TRUNCATE table migrations`;
    await sql.end();
}

module.exports = {
    resetMigrations
}
